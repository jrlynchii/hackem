package org.insomnia.hackem;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;

public class ReturnLocation{
	
	// Define human readable names
		private static final String[] ACCURACY = { "invalid", "n/a", "fine", "coarse" };
		private static final String[] POWER = { "invalid", "n/a", "low", "medium",
		"high" };
		private static final String[] STATUS = { "out of service",
			"temporarily unavailable", "available" };
		private static final String[] GPS_EVENTS = {"GPS event started", "GPS event stopped",
			"GPS event first fix", "GPS event satellite status"};

		private LocationManager mgr;
		private String best;
		private GpsStatus gps;
		private PowerManager.WakeLock wakeLock;
		
		public ReturnLocation(Activity activity) {
			mgr = (LocationManager) activity.getSystemService(Activity.LOCATION_SERVICE);
			best = mgr.getBestProvider(new Criteria(), true);
		}
		
		public float getLatitude(){
			mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 10, locationListener);
			mgr.addGpsStatusListener(gpsStatusListener);
			return (float) mgr.getLastKnownLocation(best).getLatitude();
		}
		
		public float getLongitude(){
			mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 10, locationListener);
			mgr.addGpsStatusListener(gpsStatusListener);
			return (float) mgr.getLastKnownLocation(best).getLongitude();
		}
		
		private LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				dumpLocation(location);
				Log.d("LocationTest", "Updated Location.");
			}

			public void onProviderDisabled(String provider) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		
		/** Write information from all location providers */
		private void dumpProviders() {
			List<String> providers = mgr.getAllProviders();
			for (String provider : providers) {
				dumpProvider(provider);
			}
		}
		
		/** Write information from a single location provider */
		private void dumpProvider(String provider) {
			LocationProvider info = mgr.getProvider(provider);
			StringBuilder builder = new StringBuilder();
			builder.append("LocationProvider[")
			.append("name=")
			.append(info.getName())
			.append(",enabled=")
			.append(mgr.isProviderEnabled(provider))
			.append(",getAccuracy=")
			.append(ACCURACY[info.getAccuracy() + 1])
			.append(",getPowerRequirement=")
			.append(POWER[info.getPowerRequirement() + 1])
			.append(",hasMonetaryCost=")
			.append(info.hasMonetaryCost())
			.append(",requiresCell=")
			.append(info.requiresCell())
			.append(",requiresNetwork=")
			.append(info.requiresNetwork())
			.append(",requiresSatellite=")
			.append(info.requiresSatellite())
			.append(",supportsAltitude=")
			.append(info.supportsAltitude())
			.append(",supportsBearing=")
			.append(info.supportsBearing())
			.append(",supportsSpeed=")
			.append(info.supportsSpeed())
			.append("]");
		}
		
		/** Describe the given location, which might be null */
		private void dumpLocation(Location location) {

		}
		
		GpsStatus.Listener gpsStatusListener = new GpsStatus.Listener() {
			public void onGpsStatusChanged(int event) {
				Log.d("Location Test", "gps status changed");
				gps = mgr.getGpsStatus(null);
				// showSats(); 
			} 
		};
}
