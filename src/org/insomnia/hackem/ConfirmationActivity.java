package org.insomnia.hackem;

import java.math.BigDecimal;
import java.util.UUID;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import android.os.Bundle;
import android.content.SharedPreferences;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal; 
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalPayment;

public class ConfirmationActivity extends Activity {

	private static final String TAG = "ConfirmationActivity";
	
	private PayPal ppObj;
	private float difference;
	private float store;
	private String recipient;
	private SharedPreferences mPrefs;
	private SharedPreferences mLeaders;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Confirmation", "Entering");
		setContentView(R.layout.confirm_difference);
	
		mPrefs = getSharedPreferences("pif_prefs", MODE_PRIVATE);
		String difficulty = mPrefs.getString("charity", "American Red Cross");
		
		if(difficulty.equals("American Red Cross"))
			recipient = "hsu64_1351943477_per@sbcglobal.net";
		else if(difficulty.equals("Breast Cancer"))
			recipient = "hsu64_1351817628_biz@sbcglobal.net";
		else if(difficulty.equals("Habitat For Humanity"))
			recipient = "hsu64_1351817557_per@sbcglobal.net";
		else if(difficulty.equals("Neil Wants Money"))
			recipient = "hsu64_1351817507_per@sbcglobal.net";
		
		Log.d("Confirmation", "Starting paypal + passed difficulty" + difficulty);
		ppObj = PayPal.initWithAppID(this.getBaseContext(), "APP-80W284485P519543T", PayPal.ENV_SANDBOX);
		ppObj.setShippingEnabled(false);
		
		Intent prev_intent = getIntent();
		
		float average = prev_intent.getFloatExtra(ProductInfoActivity.AVERAGE_PRICE, 0.00f);
		store = prev_intent.getFloatExtra(ProductInfoActivity.STORE_PRICE, 0.00f);
		difference = average - store;
		
		float max = mPrefs.getFloat("max_transaction_limit", (float) 1.0);
		
		if(difference > max)
			difference = max;
		
		TextView avg = (TextView) findViewById(R.id.avg_price_number);
		avg.setText("" + average);
		
		TextView diff = (TextView) findViewById(R.id.donation_amt_number);
		diff.setText("" + difference);
		
		CheckoutButton launchPayPalButton1 = 
				ppObj.getCheckoutButton(this, PayPal.BUTTON_278x43, CheckoutButton.TEXT_DONATE);
		RelativeLayout.LayoutParams params1 = 
				new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params1.bottomMargin = 10;
		launchPayPalButton1.setLayoutParams(params1);
		launchPayPalButton1.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				goToDonateDifference(v);
			}
		});
		((RelativeLayout)findViewById(R.id.relative_paypal1)).addView(launchPayPalButton1);
		
		CheckoutButton launchPayPalButton2 = 
				ppObj.getCheckoutButton(this, PayPal.BUTTON_278x43, CheckoutButton.TEXT_DONATE);
		RelativeLayout.LayoutParams params2 = 
				new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params2.bottomMargin = 10;
		launchPayPalButton2.setLayoutParams(params2);
		launchPayPalButton2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				goToDonateRoundUp(v);	
			}
		});
		((RelativeLayout)findViewById(R.id.relative_paypal2)).addView(launchPayPalButton2);
		
	}
	
	
	
	public void goToDonateRoundUp(View view) {
//		TODO replace DisplayMessageActivity with whatever activity we end up creating
		PayPalPayment newPayment = new PayPalPayment();
		newPayment.setSubtotal(BigDecimal.valueOf(Math.ceil(store)));
		newPayment.setCurrencyType("USD");
		newPayment.setRecipient(recipient);
		newPayment.setMerchantName("CheeseBunnies");
		Log.d(TAG, "donate round clicked");
		Intent paypalIntent = PayPal.getInstance().checkout(newPayment, this);
		this.startActivityForResult(paypalIntent, 1);
	}
	
	public void goToDonateDifference(View view) {
//		TODO replace DisplayMessageActivity with whatever activity we end up creating
		PayPalPayment newPayment = new PayPalPayment();
		newPayment.setSubtotal(BigDecimal.valueOf(difference));
		newPayment.setCurrencyType("USD");
		newPayment.setRecipient(recipient);
		newPayment.setMerchantName("CheeseBunnies");
		Log.d(TAG, "donatedifference clicked");
		Intent paypalIntent = PayPal.getInstance().checkout(newPayment, this);
		this.startActivityForResult(paypalIntent, 1);
	}
	
	
	public void goToMain(View view) {
//		TODO replace DisplayMessageActivity with whatever activity we end up creating
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
		case Activity.RESULT_OK:
			//The payment succeeded
			String payKey = data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
			SharedPreferences.Editor ed = mLeaders.edit();
			ed.putFloat(UUID.randomUUID().toString(), difference);
			ed.commit();
			Log.d(TAG, "Result ok");
			//Tell the user their payment succeeded
			
			break;
		case Activity.RESULT_CANCELED:
			//The payment was canceled
			//Tell the user their payment was canceled
			Log.d(TAG, "Result cancelled");
			break;
		case PayPalActivity.RESULT_FAILURE:
			//The payment failed -- we get the error from the EXTRA_ERROR_ID and EXTRA_ERROR_MESSAGE
			String errorID = data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
			String errorMessage = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
			Log.d(TAG, "Result failure: "+errorMessage);
			//Tell the user their payment was failed.
		}
		goToMain(null);
	}
	
}
