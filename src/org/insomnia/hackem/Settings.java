package org.insomnia.hackem;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Settings extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState); 
		getPreferenceManager().setSharedPreferencesName("pif_prefs");
		addPreferencesFromResource(R.xml.preferences); 
		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); 
		final ListPreference difficultyLevelPref = (ListPreference) findPreference("charity");
		final EditTextPreference victoryMessagePref = (EditTextPreference) findPreference("max_transaction_limit");
		
		
		String difficulty = prefs.getString("charity", getResources().getString(R.string.difficulty_expert)); 
		float victoryMessage = prefs.getFloat("max_transaction_limit", (float) 1.0);
		
		victoryMessagePref.setSummary("" + victoryMessage);
		difficultyLevelPref.setSummary((CharSequence) difficulty); 
		difficultyLevelPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() { 
			public boolean onPreferenceChange(Preference preference, Object newValue) { 
				difficultyLevelPref.setSummary((CharSequence) newValue); 
				// Since we are handling the pref, we must save it
				SharedPreferences.Editor ed = prefs.edit(); 
				ed.putString("charity", newValue.toString()); 
				ed.commit();     
				return true; 
			}
		});
		
		victoryMessagePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() { 
			public boolean onPreferenceChange(Preference preference, Object newValue) { 
				victoryMessagePref.setSummary((CharSequence) newValue); 
				// Since we are handling the pref, we must save it
				SharedPreferences.Editor ed = prefs.edit(); 
				ed.putFloat("max_transaction_limit", (Float) newValue); 
				ed.commit();     
				return true; 
			} 
		});


	}
	
	
	
}