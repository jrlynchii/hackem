package org.insomnia.hackem;

import org.insomnia.hackem.util.MiloUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


public class ProductInfoActivity extends Activity{
	public static final String AVERAGE_PRICE = "average_price";

	public static final String STORE_PRICE = "store_price";

	private static final String TAG = "ProductInfoActivity";
	
	float mPrice;
	float mAvg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productinfo);

		ReturnLocation location = new ReturnLocation(this);
		float lat = location.getLatitude();
		float lng = location.getLongitude();
		Log.d(TAG, "lat long:"+lat+" "+lng);
		String barcode = getIntent().getStringExtra(MainActivity.MAINACTIVITY_BARCODE);
		Log.d(TAG, "barcode: "+barcode);
//		mAvg = new MiloUtils().pingMilo(30.307689f, -97.729912f, "022000159335");
		mAvg = new MiloUtils().pingMilo(lat, lng, barcode);
//		Toast.makeText(getApplicationContext(), avg+"", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "avg: "+mAvg);
	}
	
	public void goToConfirmationActivity(View view) {
		Log.d(TAG, "firing button");
		EditText et = (EditText) findViewById(R.id.editText1);
		String input = et.getText().toString();
        mPrice = Float.parseFloat(input);
		Intent intent = new Intent(this, ConfirmationActivity.class);
		intent.putExtra(STORE_PRICE, mPrice);
		intent.putExtra(AVERAGE_PRICE, mAvg);
		startActivity(intent);
	}
}
