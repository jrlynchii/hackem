package org.insomnia.hackem;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity;
import android.content.SharedPreferences;

public class ScoreboardActivity extends Activity {

	private static final String TAG = "ScoreBoardActivity";
	private SharedPreferences mPrefs;
	private DecimalFormat df = new DecimalFormat("0.00");


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scoreboard);

		mPrefs = getSharedPreferences("leaderboard", MODE_PRIVATE);

		ListView listView = (ListView) findViewById(R.id.score_list);


		Map<String, ?> scoreMap = mPrefs.getAll();

		if (scoreMap.isEmpty()) {
			fakescores();
			scoreMap = mPrefs.getAll();
		}
		
		Float[] scores = scoreMap.values().toArray(new Float[0]);
		String[] products = scoreMap.keySet().toArray(new String[0]);

		float total = sum(scores);
		TextView tv = (TextView) findViewById(R.id.total_textview);
		tv.setText("Total donations: $"+df.format(total));
		
		String[] values = buildScoreList(scores, products);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, values);

		listView.setAdapter(adapter);
	}

	private float sum(Float[] scores) {
		int sum =0;
		for(float f: scores){
			sum+=f;
		}
		return sum;
	}

	//Bad code in the interest of finishing tonight...
	private String[] buildScoreList(Float[] scores, String[] products) {
		
		
		Map<String, Float> unsortedMap = new HashMap<String, Float>();
		for(int i=0; i<scores.length; i++){
			unsortedMap.put(products[i], scores[i]);
		}
		
		Map<String, Float> sortedMap = new TreeMap<String, Float>(new ValueComparator(unsortedMap));
		sortedMap.putAll(unsortedMap);

		int length = sortedMap.size();
		String[] scoreStrings = new String[length];
		
		int i=0;
		String product;
		float price;
		for(Entry<String, Float> entry: sortedMap.entrySet()){
			product = entry.getKey();
			price = entry.getValue();
//			scoreStrings[(length-1)-i] = product+": $"+df.format(price);
			scoreStrings[(length-1)-i] = "$"+df.format(price);
			i++;
		}
		
		return scoreStrings;
	}
	
	class ValueComparator implements Comparator<String> {

	    Map<String, Float> base;
	    public ValueComparator(Map<String, Float> base) {
	        this.base = base;
	    }

	    // Note: this comparator imposes orderings that are inconsistent with equals.    
	    public int compare(String a, String b) {
	        if (base.get(a) <= base.get(b)) {
	            return -1;
	        } else {
	            return 1;
	        } // returning 0 would merge keys
	    }
	}

	private void fakescores() {
		SharedPreferences.Editor ed = mPrefs.edit();
		for (int i = 0; i < 20; i++) {
			ed.putFloat("product"+i,  i*1.01f);
		}
		ed.putFloat("product21",  15.00f);
		ed.commit();
	}
}
