package org.insomnia.hackem.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class MiloUtils {

	private static final String TAG = "MiloUtils";

	private static final String QUERY_TEMPLATE = "https://api.x.com/milo/v3/products?key=%s&q=upc:%s&latitude=%s&longitude=%s&show=PminPmax&show_defaults=false";
	private static final String API_KEY = "218f19a57ad893ceaba3d80ff918c6b3";

	// private static final HttpClient httpclient = new DefaultHttpClient();

	public float pingMilo(float lat, float lng, String upc) {
		Log.d(TAG, "Pinging milo");
		String query = String.format(QUERY_TEMPLATE, API_KEY, upc, lat, lng);
		Log.d(TAG, "Query string: " + query);
		try {
			float avg = new GetTask().execute(new URL(query)).get();
			return avg;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return -1;
	}

	private class GetTask extends AsyncTask<URL, Void, Float> {
		protected Float doInBackground(URL... urls) {
			float price = 0;

			HttpURLConnection urlConnection = null;
			try {
				Log.d(TAG, "Http request sending");
				urlConnection = (HttpURLConnection) urls[0].openConnection();
				InputStream in = new BufferedInputStream(
						urlConnection.getInputStream());
				price = readStream(in);

			} catch (MalformedURLException e1) {
				Log.e(TAG, e1.getMessage());
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			} finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
			}
			return price;
		}

		private float readStream(InputStream in) {
			Scanner scan = new Scanner(in).useDelimiter("[^0-9]+");
			float sum = 0;
			int count =0;
			int current;
			while(scan.hasNextInt()){
				current = scan.nextInt();
				sum+=current;
				count++;
				Log.d(TAG, ""+current);
			}
			float avg = sum/Math.max(count, 1)/100.0f;
//			return avg;
			return Math.max(avg, 2.45f);
		}

	}
}
