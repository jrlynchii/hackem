package org.insomnia.hackem;

//import com.ebay.rlsample.R;
//import com.ebay.rlsample.RLSample;
//import com.ebay.rlsample.RLSampleScannerActivity;

import java.util.ArrayList;

import org.insomnia.hackem.BarcodeListAdapter;
import org.insomnia.hackem.Settings;

import com.ebay.redlasersdk.BarcodeResult;
import com.ebay.redlasersdk.BarcodeScanActivity;

import org.insomnia.hackem.R;
import org.insomnia.hackem.util.MiloUtils;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	public static final String MAINACTIVITY_BARCODE = "MAINACTIVITY_BARCODE";
	BarcodeListAdapter listAdapter;
	private static final String SAVED_INSTANCE_LIST = "BarcodeList";
	private static final String TAG = "MainActivity";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		

//		ArrayList<BarcodeResult> savedValues = null;
//		if (savedInstanceState != null)
//        {
//        	savedValues = (ArrayList<BarcodeResult>) savedInstanceState.getSerializable(SAVED_INSTANCE_LIST);
//        }
//        ListView barcodeList = (ListView) findViewById(R.id.listView1);
//		listAdapter = new BarcodeListAdapter(this, savedValues);
//		barcodeList.setAdapter(listAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void goToAbout(View view) {
//		TODO replace DisplayMessageActivity with whatever activity we end up creating
		Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
	}

	public void goToSettings(View view) {
		Intent intent = new Intent(this, Settings.class);
		startActivity(intent);
	}

	public void goToScoreboard(View view) {
		Intent intent = new Intent(this, ScoreboardActivity.class);
		startActivity(intent);
	}

	public void goToScan(View view) {
//		TODO replace DisplayMessageActivity with whatever activity we end up creating
		try {
			Intent intent = new Intent(this, ScanActivity.class);
			startActivityForResult(intent, 0);
		} catch(Exception e) {
			Log.d("MainActivity.goToScan",e.getLocalizedMessage()+" "+e.getCause());
		}
	}
	
	public void temp(View view) {
//		TODO replace DisplayMessageActivity with whatever activity we end up creating
		
			Intent intent = new Intent(this, ConfirmationActivity.class);
			startActivity(intent);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		
		Log.d(TAG, "onActivityResult called.");
		
		// We came from the scanning activity; the return intent contains a RESULT_EXTRA key
		// whose value is an ArrayList of BarcodeResult objects that we found while scanning.
		// Get the list of objects and add them to our list view.
		if (resultCode == RESULT_OK) 
		{			
			ArrayList<BarcodeResult> barcodes = data.getParcelableArrayListExtra(BarcodeScanActivity.RESULT_EXTRA);
			if (barcodes != null)
			{
				Log.d(TAG, "Got back barcodes.");

				if(barcodes.size() > 1){
					Log.d(TAG, "Too many barcodes");
				} else if (barcodes.size() < 1){
					Log.d(TAG, "No barcodes");
				} else {
					Intent intent = new Intent(this, ProductInfoActivity.class);
					intent.putExtra(MAINACTIVITY_BARCODE, barcodes.get(0).barcodeString);
					startActivity(intent);
				}
					
				
			} else {
				Log.d(TAG, "Didn't get back barcodes");
			}
			
		}
	}
}
